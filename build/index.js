const sass = require('node-sass')
const glob = require('glob')
const fs = require('fs')
const path = require('path')
const colors = require('colors/safe')
const parser = require('argv-parser')

// 設定colors套件的顏色對應
colors.setTheme({
  silly: 'rainbow',
  input: 'grey',
  verbose: 'cyan',
  prompt: 'grey',
  info: 'green',
  data: 'grey',
  help: 'cyan',
  warn: 'yellow',
  debug: 'blue',
  error: 'red'
})

// 利用argv-parser套件解析傳入參數值
const data = parser.parse(process.argv, {
  rules: {
    settings: {
      minify: true,
      'source-map': true,
      'input-folder': '',
      'output-folder': '',
      compass: false
    }
  }
})
console.log(colors.verbose(`minify(是否最小化[預設為true]) => ${data.parsed.minify}`))
console.log(colors.verbose(`source-map(是否開啟source map[預設為true]) => ${data.parsed['source-map']}`))
console.log(colors.verbose(`*input-folder(Sass原始程式的資料夾[必填]) => ${data.parsed['input-folder']}`))
console.log(colors.verbose(`*output-folder(輸出css的資料夾[必填]) => ${data.parsed['output-folder']}`))
console.log(colors.verbose(`importer compass(是否載入compass[預設為false]) => ${data.parsed.compass}\n`))
console.log(colors.silly(sass.info) + `\n`)

// 取得npm scripts傳入的參數
const inputFolder = data.parsed['input-folder']
const outputFolder = data.parsed['output-folder']
const minify = data.parsed.minify
const sourceMap = data.parsed['source-map']
const compass = data.parsed.compass
const importer = compass ? require('compass-importer') : undefined

// 錯誤處理，必需指定Sass程式碼所在的資料夾與輸出css的資料夾
if (!(inputFolder && outputFolder)) {
  throw Error('Please setting input/output folder.(--input-folder, --output-folder)')
}

// 修改輸出檔案的路徑與檔名
function addMinExtension (fileName) {
  fileName = fileName.substr(0, fileName.lastIndexOf('.')) + (minify ? '.min.css' : '.css')
  // 修改路徑至輸出資料夾
  return fileName.replace(inputFolder, outputFolder)
}

// 確保寫入檔案時資料夾已存在，避免發生錯誤
function ensureDirectoryExistence (filePath) {
  var dirname = path.dirname(filePath)
  if (fs.existsSync(dirname)) {
    return true
  }
  ensureDirectoryExistence(dirname)
  fs.mkdirSync(dirname)
}

// 儲存檔案
function saveFile (dataToSave, filePath, message, callback) {
  fs.unlink(filePath, (err) => {
    if (err && err.code !== 'ENOENT') { throw err }

    ensureDirectoryExistence(filePath)

    let options = { flag: 'w' }
    fs.writeFile(filePath, dataToSave, options, (err) => {
      if (err) { console.log(colors.error(err)) } else {
        console.log(colors.info(message))
        callback && callback()
      }
    })
  })
}

// 使用glob套件取得所有要輸出的.scss檔案(sass資料夾內非_開頭的scss檔案))
const files = glob.sync(`${inputFolder}**/!(_*).scss`)
// 執行.scss檔案轉譯為.css
files.forEach(function (file) {
  const outputFileName = addMinExtension(file)
  // node-sass產出css的方法(參考node-sass的options說明)
  sass.render({
    file: file,
    outFile: outputFileName,
    sourceMap: sourceMap,
    outputStyle: minify ? 'compressed' : 'nested',
    importer: importer
  }, function (error, result) {
    if (error) {
      // 發生錯誤時的處理
      let errorToLog = error.formatted || error
      console.log(colors.error(errorToLog))
    } else {
      // 轉譯成功，產生.css檔案
      let compiledMessage = 'Saved compiled file ' + outputFileName
      saveFile(result.css.toString(), outputFileName, compiledMessage)

      if (result.map) {
        // 產生.map檔案
        let mapFileName = outputFileName + '.map'
        let savedMessage = 'Saved map file ' + mapFileName
        saveFile(result.map.toString(), mapFileName, savedMessage)
      }
    }
  })
})
